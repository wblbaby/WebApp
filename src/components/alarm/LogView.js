import {almAPI} from '@/api/restfulAPI'
import * as glbData from '@/js/globalData'
import moment from 'moment'

let logView = {
  methods: {
    timeFormat (ts) {
      let rcTs = 'none'
      if (ts != null) {
        rcTs = `${moment(ts).format('YYYY-MM-DD')}\n${moment(ts).format('HH:mm:ss')}`
      }
      return rcTs
    },
    disableViewLog (type) {
      return type !== 'app status' && type !== 'app event'
    },
    viewErrorLog (row) {
      row.loading = true
      almAPI.getAlarmLog(row.sourceKey, row.createTs).then(data => {
        delete row.loading
        if (data != null && data.length > 0) {
          let logDatas = data
          let errLog = this.makeLogWithTemplete(logDatas)
          glbData.openWindow(`ErrorLog-#${row.sourceName}`, null, errLog)
        } else {
          let msg = {
            message: `${row.sourceName} not have error log!`,
            type: 'warning'
          }
          this.setCurMsg(msg)
        }
      }).catch(error => {
        delete row.loading
        if (error.response.data != null &&
          error.response.data.errorMsg != null &&
          error.response.data.errorMsg.length > 0) {
          let msg = {
            message: `Get ${row.sourceName} error log failed, cause: ${error.response.data.errorMsg}`,
            type: 'error'
          }
          this.setCurMsg(msg)
        }
      })
    },
    makeLogWithTemplete (logDatas) {
      let head = `<head>
        <style type="text/css">
          .log-container {
            height: 100vh;
            display:flex;
            flex-direction: column;
            background-color: #eaedef;
          }
          .log-title {
            height: 20px;
            font-weight: bold;
            font-size: 1em;
            padding: 10px;
            border: 1px solid #dfe6ec;
            border-radius: 5px 5px 0 0;
            margin: 10px 10px 2px 10px;
            background-color: #ffffff;
          }
          .log-content{
            flex-grow: 1;
            padding: 10px;
            border: 1px solid #dfe6ec;
            border-radius: 0 0 5px 5px;
            margin: 2px 10px 10px 10px;
            background-color: #ffffff;
          }
          .log-item{
            font-size: 1em;
          }
          .log-item-prefix {
              color: #176799
          }
          .log-item-content-red {
              color: ff434c;
          }
        </style>
      </head>`
      let content = ''
      for (let logData of logDatas) {
        content = `${content}<div class='log-item'>
          <span class='log-item-prefix'>[${moment(logData.timestamp).format('YYYY-MM-DD HH:mm:ss')}]</span>
          <span class='log-item-prefix'>[${logData.sourceType}/${logData.sourceInstance}]</span>
          <span class='${logData.messageType.toLowerCase() === 'err' ? 'log-item-content-red' : ''}'>[${logData.messageType}]</span>
          <span class='${logData.messageType.toLowerCase() === 'err' ? 'log-item-content-red' : ''}'>${logData.message}</span>
        </div>`
      }
      let body = `<body style='margin:0'>
        <div class='log-container'>
          <div class='log-title'>logs</div>
          <div class='log-content'>${content}</div>
        </div>
      </body>`
      let logHtml = `${head}${body}`
      return logHtml
    }
  }
}

export default logView
