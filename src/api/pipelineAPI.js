import ajaxRest from './ajaxBase'

const CONTROLLER_PREFIX = '/pipelinecontroller'

function listPpl (prjID, offset, count) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/all/pipelines`,
      method: 'GET',
      data: {
        projectId: prjID,
        startNum: offset,
        count: count
      },
      timeout: {client: 10000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function addPpl (prjID, { name, gitUrl, branch, desc, active }) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/pipelines`,
      method: 'POST',
      data: {
        projectId: prjID,
        name: name,
        gitUrl: gitUrl,
        branch: branch,
        description: desc,
        active: active
      },
      timeout: {client: 10000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function updatePpl (prjID, { id, name, gitUrl, branch, desc, active }) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/pipelines`,
      method: 'PUT',
      data: {
        projectId: prjID,
        id: id,
        name: name,
        gitUrl: gitUrl,
        branch: branch,
        description: desc,
        active: active
      },
      timeout: {client: 10000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function applyPplParams (id, parameters) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/pipelines`,
      method: 'PUT',
      data: {
        id: id,
        parameters: parameters
      },
      timeout: {client: 120000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

export default { listPpl, addPpl, updatePpl, applyPplParams }
