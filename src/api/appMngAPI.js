import ajaxRest from './ajaxBase'
const CONTROLLER_PREFIX = '/appcontroller'
function getAppsInfo (prjId) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/byprojectid/applifes`,
      method: 'GET',
      data: {
        projectId: prjId
      },
      timeout: {client: 10000}
    }
    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}
export default {getAppsInfo}
