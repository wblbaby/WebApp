import ajaxRest from './ajaxBase'

const CONTROLLER_PREFIX = '/stagecontroller'
const RES_CONTROLLER_PREFIX = '/resourcecontroller'
const RPT_CONTROLLER_PREFIX = '/reportcontroller'

function listStageWithBld (bldId, offset, count) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/all/stages`,
      method: 'GET',
      data: {
        buildId: bldId,
        startNum: offset,
        count: count
      }
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function queryReport (index, res, rpts, resolve) {
  if (index < res.length) {
    let curRes = res[index]
    let id = parseInt(curRes.value)
    if (curRes.type === 'report' && !curRes.input && !isNaN(id)) {
      let params = {
        url: `${RPT_CONTROLLER_PREFIX}/byid/reports`,
        method: 'GET',
        data: {
          id: curRes.value
        }
      }
      ajaxRest(params).then(data => {
        let rpt = data
        rpts.push(rpt)
        queryReport(++index, res, rpts, resolve)
      }).catch(() => {
        queryReport(++index, res, rpts, resolve)
      })
    } else queryReport(++index, res, rpts, resolve)
  } else resolve(rpts)
}

function getStageReport (stgId) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${RES_CONTROLLER_PREFIX}/all/resources`,
      method: 'GET',
      data: {
        stageId: stgId
      }
    }

    ajaxRest(params).then(data => {
      let res = data.data
      let rep = []
      if (res != null && res.length > 0) {
        queryReport(0, res, rep, resolve)
      } else resolve(rep)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

export default {listStageWithBld, getStageReport}
