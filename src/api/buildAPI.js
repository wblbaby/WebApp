import ajaxRest from './ajaxBase'

const CONTROLLER_PREFIX = '/buildcontroller'

function listBuild (pplID, offset, count) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/all/builds`,
      method: 'GET',
      data: {
        pipelineId: pplID,
        startNum: offset,
        count: count
      },
      timeout: {client: 30000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function addAndExecBuild (pplID, jobName, pplParams) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/builds/add/exec`,
      method: 'POST',
      data: {
        jobName: jobName,
        parameters: pplParams
      },
      timeout: {client: 30000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function createBuild (jobName) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/builds`,
      method: 'POST',
      data: {
        jobName: jobName
      },
      timeout: {client: 30000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function execBuild (pplID, jobName, pplParams) {
  let p = new Promise((resolve, reject) => {
    createBuild(jobName).then(buildInfo => {
      let execParam = [...pplParams, {
        name: 'BuildID',
        value: `${buildInfo.id}`,
        type: 'string'
      }]
      let params = {
        url: `${CONTROLLER_PREFIX}/exec/builds`,
        method: 'POST',
        data: {
          pipelineId: pplID,
          parameters: execParam
        },
        timeout: {client: 30000}
      }

      ajaxRest(params).then(data => {
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function getBuildLog (buildId) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/logs`,
      method: 'GET',
      data: {
        id: buildId
      },
      timeout: {server: 5000, client: 30000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

export default { listBuild, createBuild, execBuild, getBuildLog, addAndExecBuild }
