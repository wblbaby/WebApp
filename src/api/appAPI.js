import ajaxRest from './ajaxBase'
let base64 = require('base-64')
let utf8 = require('utf8')
const CONTROLLER_PREFIX = '/appcontroller'
function listAppsCfg () {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/all/apps`,
      method: 'GET',
      timeout: {client: 10000}
    }
    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}
function updatePrjApps (prjId, apps) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/apps`,
      method: 'PUT',
      data: {
        projectId: prjId,
        apps: apps
      },
      timeout: {client: 10000}
    }
    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}
function listAppStatus () {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/all/appinfos`,
      method: 'GET',
      timeout: {client: 10000}
    }
    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}
function getChangeLog (gitUrl) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/changelogs`,
      method: 'GET',
      data: {
        gitUrl: gitUrl
      },
      timeout: {client: 10000}
    }
    ajaxRest(params).then(data => {
      let bytes = base64.decode(data)
      let text = utf8.decode(bytes)
      resolve(text)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}
export default {listAppsCfg, updatePrjApps, listAppStatus, getChangeLog}
