import projectAPI from './projectAPI'
import pipelineAPI from './pipelineAPI'
import buildAPI from './buildAPI'
import stageAPI from './stageAPI'
import appAPI from './appAPI'
import loginAPI from './loginAPI'
import credentialAPI from './credentialAPI.js'
import profileAPI from './profileAPI'
import appMngAPI from './appMngAPI'
export {
  projectAPI as prjAPI,
  pipelineAPI as pplAPI,
  buildAPI as bldAPI,
  stageAPI as stgAPI,
  appAPI,
  loginAPI,
  credentialAPI as credAPI,
  profileAPI as pfAPI,
  appMngAPI
}
