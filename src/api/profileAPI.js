import ajaxRest from './ajaxBase'

const CONTROLLER_PREFIX = '/usercontroller'

function getUserInfo (userID) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/byid/users`,
      method: 'GET',
      data: {
        id: userID
      },
      timeout: {client: 10000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

export default { getUserInfo }
