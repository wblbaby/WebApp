import ajaxRest from './ajaxBase'

const CONTROLLER_PREFIX = '/usercontroller'

function login (user, pwd, tokenTs) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/login`,
      method: 'POST',
      data: {
        userName: user,
        password: pwd,
        tokenTs: tokenTs
      },
      timeout: {client: 2000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

export default { login }
