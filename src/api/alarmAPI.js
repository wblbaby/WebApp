import ajaxRest from './ajaxBase'
const CONTROLLER_PREFIX = '/alarmcontroller'
function listRTAlarm (offset, count) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/real/all/alarms`,
      method: 'GET',
      data: {
        startNum: offset,
        count: count
      },
      timeout: {client: 10000}
    }
    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}
function listHstAlarm (offset, count) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/history/all/alarms`,
      method: 'GET',
      data: {
        startNum: offset,
        count: count
      },
      timeout: {client: 10000}
    }
    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}
function getAlarmLog (appId, almCreatTs) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `/applogcontroller/bytime/logs`,
      method: 'GET',
      data: {
        appId: appId,
        currentTime: almCreatTs
      },
      timeout: {client: 10000}
    }
    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}
export default {listRTAlarm, listHstAlarm, getAlarmLog}
