import ajaxRest from './ajaxBase'

const CONTROLLER_PREFIX = '/credentialcontroller'

function getCredentialItems () {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/credentials`,
      method: 'GET',
      timeout: {client: 2000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function addCredential (userId, cred) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/credentials`,
      method: 'POST',
      data: {
        userId: userId,
        ...cred
      },
      timeout: {client: 2000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function updateCredential (cred) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/credentials`,
      method: 'PUT',
      data: cred,
      timeout: {client: 2000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function deleteCredential (credId) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/credentials?id=${credId}`,
      method: 'DELETE',
      timeout: {client: 2000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

function getCredentials (userId, type) {
  let p = new Promise((resolve, reject) => {
    let params = {
      url: `${CONTROLLER_PREFIX}/all/credentials`,
      method: 'GET',
      data: {
        userId: userId,
        type: type
      },
      timeout: {client: 2000}
    }

    ajaxRest(params).then(data => {
      resolve(data)
    }).catch(error => {
      reject(error)
    })
  })
  return p
}

export default {getCredentialItems, getCredentials, addCredential, updateCredential, deleteCredential}
