export const MAIN_MAIN = 'main'

export const PRJ_MODULE_NAME = 'projectModule'
export const PRJ_MAIN = 'project'
export const PRJ_CONFIG = 'prjConfiguration'

export const APP_MODULE_NAME = 'appModule'
export const APP_MAIN = 'app'
export const APP_LIFECYCLE = 'appLifecycle'
export const APP_ALARMSTATISTIC = 'appAlarmStatistic'

export const PPL_MODULE_NAME = 'pipelineModule'
export const PPL_MAIN = 'pipeline'

export const USER_MANUAL = 'User Manual'

export const PPL_DETAIL = 'pplDetail'
export const PPL_CONFIG = 'pplConfiguration'
export const PPL_BUILDHISTORY = 'pplBuildHistory'

export const HLT_MODULE_NAME = 'healthyModule'
export const HLT_MAIN = 'healthy'
export const HLT_CFDP = 'hltCFDeployStatus'

export const ALM_MODULE_NAME = 'alarmModule'
export const ALM_MAIN = 'alarm'
export const ALM_REALTIME = 'realTime'
export const ALM_HISTORY = 'history'

export const LGI_MODULE_NAME = 'loginModule'
export const LGI_MAIN = 'login'

export const USR_MODULE_NAME = 'userModule'
export const USR_MAIN = 'user'
export const USR_PROFILE = 'profile'
export const USR_CREDENTIAL = 'credential'

export function belongModule (pageName) {
  let moduleName = ''
  switch (pageName) {
    case PRJ_MAIN:
    case PRJ_CONFIG:
      moduleName = PRJ_MODULE_NAME
      break
    case PPL_MAIN:
    case PPL_DETAIL:
    case PPL_CONFIG:
    case PPL_BUILDHISTORY:
      moduleName = PPL_MODULE_NAME
      break
    case APP_LIFECYCLE:
    case APP_ALARMSTATISTIC:
    case APP_MAIN:
      moduleName = APP_MODULE_NAME
      break
    case HLT_MAIN:
      moduleName = HLT_MODULE_NAME
      break
    case ALM_MAIN:
      moduleName = ALM_MAIN
      break
    case ALM_REALTIME:
      moduleName = ALM_REALTIME
      break
    case ALM_HISTORY:
      moduleName = ALM_HISTORY
      break
    case LGI_MAIN:
      moduleName = LGI_MODULE_NAME
      break
    case USR_MAIN:
      moduleName = USR_MAIN
      break
    case USR_PROFILE:
      moduleName = USR_PROFILE
      break
    case USR_CREDENTIAL:
      moduleName = USR_CREDENTIAL
      break
  }
  return moduleName
}

export function getMoudleMain (moduleName) {
  let mainName = ''
  switch (moduleName) {
    case PRJ_MODULE_NAME:
      mainName = PRJ_MAIN
      break
    case PPL_MODULE_NAME:
      mainName = PPL_MAIN
      break
    case APP_MODULE_NAME:
      mainName = APP_MAIN
      break
    case HLT_MODULE_NAME:
      mainName = HLT_MAIN
      break
    case ALM_MODULE_NAME:
      mainName = ALM_MAIN
      break
    case ALM_REALTIME:
      mainName = ALM_REALTIME
      break
    case ALM_HISTORY:
      mainName = ALM_HISTORY
      break
    case LGI_MODULE_NAME:
      mainName = LGI_MAIN
      break
    case USR_MODULE_NAME:
      mainName = USR_MAIN
      break
    case USR_PROFILE:
      mainName = USR_PROFILE
      break
    case USR_CREDENTIAL:
      mainName = USR_CREDENTIAL
      break
  }
  return mainName
}

export function fistLetterUpper (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

export const GIT_CRED_TYPE = 'git_credentials'
export const CF_CRED_TYPE = 'cf_credentials'
export const STRING_TYPE = 'string'
export const BOOLEAN_TYPE = 'boolean'
export const PASSWORD_TYPE = 'password'

function getVerSection (ver, pos) {
  let numInfo = {
    num: 0,
    pos: pos
  }
  if (pos < ver.length) {
    let numStr = ''
    while (pos < ver.length && ver.charAt(pos) !== '.') {
      numStr += ver.charAt(pos)
      pos++
    }
    numInfo.num = parseInt(numStr)
    numInfo.pos = pos
  }
  return numInfo
}

export function compareVersion (ver1, ver2) {
  if (ver1 == null || ver1.length === 0 || ver2 == null || ver2.length === 0) return -2
  else {
    let v1Pos = 0
    let v2Pos = 0
    while (v1Pos < ver1.length && v2Pos < ver2.length) {
      let v1NumInfo = getVerSection(ver1, v1Pos)
      let v2NumInfo = getVerSection(ver2, v2Pos)
      if (v1NumInfo.num < v2NumInfo.num) return -1
      else if (v1NumInfo.num > v2NumInfo.num) return 1
      else {
        v1Pos = v1NumInfo.pos < ver1.length ? v1NumInfo.pos + 1 : v1NumInfo.pos
        v2Pos = v2NumInfo.pos < ver2.length ? v2NumInfo.pos + 1 : v2NumInfo.pos
      }
    }
    if (v1Pos === ver1.length && v2Pos === ver2.length) return 0
    if (v1Pos < ver1.length) return 1
    else return -1
  }
}

export function openWindow (title, url, log) {
  let curUrl = ''
  if (url != null) curUrl = url
  let win = window.open(curUrl)
  if (log != null) win.document.write(log)
  win.document.title = title
  win.focus()
}

export const APP_LC_TAB_NAME = 'lc'
export const APP_AS_TAB_NAME = 'as'
