import {
    Button,
    Row,
    Col,
    Menu,
    MenuItem,
    Submenu,
    Select,
    Option,
    Dropdown,
    DropdownMenu,
    DropdownItem,
    Table,
    TableColumn,
    Pagination,
    Dialog,
    Form,
    FormItem,
    Input,
    Checkbox,
    Tabs,
    TabPane,
    Tooltip,
    Tag,
    Loading,
    Switch,
    Transfer,
    Popover,
    Carousel,
    CarouselItem
} from 'element-ui'

import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

function importElement (Vue) {
  locale.use(lang)

  Vue.use(Button)
  Vue.use(Row)
  Vue.use(Col)
  Vue.use(Menu)
  Vue.use(MenuItem)
  Vue.use(Submenu)
  Vue.use(Select)
  Vue.use(Option)
  Vue.use(Dropdown)
  Vue.use(DropdownMenu)
  Vue.use(DropdownItem)
  Vue.use(Table)
  Vue.use(TableColumn)
  Vue.use(Pagination)
  Vue.use(Dialog)
  Vue.use(Form)
  Vue.use(FormItem)
  Vue.use(Input)
  Vue.use(Checkbox)
  Vue.use(Tabs)
  Vue.use(TabPane)
  Vue.use(Tooltip)
  Vue.use(Tag)
  Vue.use(Loading)
  Vue.use(Switch)
  Vue.use(Transfer)
  Vue.use(Popover)
  Vue.use(Carousel)
  Vue.use(CarouselItem)
}

export default importElement
