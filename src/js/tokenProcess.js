import {almAPI} from '@/api/restfulAPI'

export const TOKEN_STG_KEY = 'devopsToken'

export function rememberToken (token) {
  localStorage.setItem(TOKEN_STG_KEY, token)
}

export function getToken () {
  let token = null
  token = sessionStorage.getItem(TOKEN_STG_KEY)
  if (token == null) {
    token = localStorage.getItem(TOKEN_STG_KEY)
    if (token) {
      sessionStorage.setItem(TOKEN_STG_KEY, token)
    }
  }
  return token
}

export function setToken (token) {
  sessionStorage.setItem(TOKEN_STG_KEY, token)
}

export function checkToken (token) {
  let p = new Promise((resolve, reject) => {
    almAPI.listRTAlarm(0, 0).then(() => {
      resolve()
    }).catch(error => {
      if (error.errCode === 401 && error.subStatus === 3) reject()
      else resolve()
    })
  })
  return p
}

export function clearToken () {
  localStorage.removeItem(TOKEN_STG_KEY)
  sessionStorage.removeItem(TOKEN_STG_KEY)
}

export function getUserInfoFromToken () {
  let user = null
  let token = getToken()
  if (token != null) {
    let payloadBStr = token.split('.')[1]
    if (payloadBStr != null) {
      let payloadStr = window.atob(payloadBStr)
      let payload = JSON.parse(payloadStr)
      user = {
        id: payload.id,
        name: payload.user,
        role: payload.role,
        loginTs: payload.iat
      }
    }
  }
  return user
}
