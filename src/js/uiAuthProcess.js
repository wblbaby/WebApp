export const ADMIN_POWER = 0x80
export const GUEST_POWER = 0x01

export const CRED_EDIT_AUTH_MASK = ADMIN_POWER
export const PPL_EDIT_AUTH_MASK = ADMIN_POWER
export const PRJ_EDIT_AUTH_MASK = ADMIN_POWER
export const PRJ_CFG_AUTH_MASK = ADMIN_POWER

export function checkUIAuth (role, mask) {
  let power = role === 'admin' ? ADMIN_POWER : GUEST_POWER
  return (power & mask) > 0
}
