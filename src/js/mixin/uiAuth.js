import * as uiAuthData from '@/js/uiAuthProcess'

let uiAuth = {
  methods: {
    checkUIAuth (role, mask) {
      return uiAuthData.checkUIAuth(role, mask)
    }
  }
}

export default uiAuth
