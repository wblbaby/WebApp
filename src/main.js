// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import importElement from '@/js/importElement.js'
import store from './store'
import { sync } from 'vuex-router-sync'
import clipboard from 'vue-clipboard2'

Vue.config.productionTip = false

Vue.use(clipboard)

sync(store, router)

importElement(Vue)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<router-view></router-view>'
})
