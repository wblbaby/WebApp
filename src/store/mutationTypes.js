export const GET_ACTIVE_PRJS = 'getActivePrjs'
export const SET_CUR_MSG = 'setCurMsg'
export const SET_CUR_ALARM = 'setCurAlarm'

export const GET_PRJS = 'getPrjs'

export const GET_PPLS = 'getPpls'
export const SET_CUR_PPL = 'setCurPpl'
export const SET_CUR_PPL_PARAMS = 'setCurPplParams'
export const SET_PPL_MANUAL_REFRESH = 'setPplManualRefresh'

export const SET_CUR_LOGIN_USER = 'setCurLoginUser'
export const SET_CUR_USER_CREDS = 'setCurCreds'

export const GET_APPS_INFO = 'getAppsInfo'
