import * as types from '@/store/mutationTypes'
import {prjAPI, credAPI} from '@/api/restfulAPI'
import * as tokenPrc from '@/js/tokenProcess'

const state = {
  user: {
    id: 0,
    name: '',
    role: '',
    loginTs: 0
  },
  curWsp: {
    id: 1,
    name: 'EI-Workspace'
  },
  activePrjs: [],
  curMessage: {
    message: '',
    type: 'success'
  },
  curAlarmCnt: 0,
  curUserCreds: []
}

const getters = {
  curPage: state => {
    return state.route.name
  }
}

const mutations = {
  [types.GET_ACTIVE_PRJS] (state, prjs) {
    state.activePrjs = prjs
  },
  [types.SET_CUR_MSG] (state, msg) {
    state.curMessage = msg
  },
  [types.SET_CUR_LOGIN_USER] (state, user) {
    state.user = user
  },
  [types.SET_CUR_USER_CREDS] (state, creds) {
    state.curUserCreds = creds
  }
}

const actions = {
  updateActivePrjs ({commit, state}) {
    let activePrjs = []
    prjAPI.getActivePrjs(state.curWsp.id).then(data => {
      activePrjs = data
      commit(types.GET_ACTIVE_PRJS, activePrjs)
    })
    .catch(error => {
      console.log(error)
    })
  },
  getLoginUserInfo ({commit, state}) {
    let user = tokenPrc.getUserInfoFromToken()
    if (user != null) commit(types.SET_CUR_LOGIN_USER, user)
  },
  getUserCreds ({commit, state}) {
    let p = new Promise((resolve, reject) => {
      credAPI.getCredentials(state.user.id, '').then(creds => {
        commit(types.SET_CUR_USER_CREDS, creds)
        resolve('OK')
      }).catch(error => {
        console.log(error)
      })
    })
    return p
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
