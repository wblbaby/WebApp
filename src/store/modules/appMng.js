import * as types from '@/store/mutationTypes'
import {appMngAPI} from '@/api/restfulAPI'

const state = {
  appsInfo: []
}

const getters = {
  getAppInfoById: (state, getters, rootState, rootGetters) => (appId) => {
    let findAppInfo = state.appsInfo.find((appInfo) => {
      return appInfo.appId === appId
    })
    if (findAppInfo !== undefined &&
      findAppInfo.latestVersion !== undefined &&
      findAppInfo.latestVersion !== null &&
      findAppInfo.latestVersion.length !== 0) {
      findAppInfo.data.nameAppend = findAppInfo.latestVersion
    }
    return findAppInfo
  }
}

const mutations = {
  [types.GET_APPS_INFO] (state, appsInfo) {
    state.appsInfo = appsInfo
  }
}

const actions = {
  getAppsInfo ({commit}, prjId) {
    let appsInfo = []
    appMngAPI.getAppsInfo(prjId).then(data => {
      appsInfo = data
      commit(types.GET_APPS_INFO, appsInfo)
    })
    .catch(error => {
      console.log(error)
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
