import * as types from '../mutationTypes'
import {pplAPI} from '../../api/restfulAPI'

const state = {
  curPpls: [],
  pplTotalSize: 0,
  curPpl: null,
  manualRefresh: 0,
  paramTypes: [
    'string',
    'git_credentials',
    'cf_credentials',
    'boolean',
    'password'
  ],
  gitCreds: [
    'ei_paas.redmine'
  ]
}

const getters = {
  cfCreds: (state, getters, rootState, rootGetters) => {
    let cfCreds = []
    for (let cred of rootState.curUserCreds) {
      if (cred.type === 'cf') {
        cfCreds.push(cred)
      }
    }
    return cfCreds
  }
}

const mutations = {
  [types.GET_PPLS] (state, {curPpls, totalSize}) {
    state.curPpls = curPpls
    state.pplTotalSize = totalSize
    if (state.manualRefresh === 1) {
      state.manualRefresh = 0
      if (state.curPpl != null) {
        let id = state.curPpl.id
        state.curPpl = state.curPpls.find(ppl => {
          return ppl.id === id
        })
      }
    }
  },
  [types.SET_CUR_PPL] (state, id) {
    if (state.curPpl == null || state.curPpl.id !== id) {
      state.curPpl = state.curPpls.find(ppl => {
        return ppl.id === id
      })
    }
  },
  [types.SET_PPL_MANUAL_REFRESH] (state, refresh) {
    state.manualRefresh = refresh
  }
}

const actions = {
  getPpls ({commit, state}, {prjId, startIndex, count}) {
    let p = new Promise((resolve, reject) => {
      pplAPI.listPpl(prjId, startIndex, count).then(data => {
        let curPpls = data.data
        let totalSize = data.totalCount
        commit(types.GET_PPLS, {curPpls, totalSize})
        resolve(curPpls)
      }).catch(error => {
        reject(error)
      })
    })
    return p
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
