const Main = () => import(/* webpackChunkName: "g-main" */ '@/components/Main')

const Project = () => import(/* webpackChunkName: "g-project" */ '@/components/project/Project')

const Pipeline = () => import(/* webpackChunkName: "g-pipeline" */ '@/components/pipeline/Pipeline')
const PplDetail = () => import(/* webpackChunkName: "g-pipeline" */ '@/components/pipeline/PplDetail')
const Configuration = () => import(/* webpackChunkName: "g-pipeline" */ '@/components/pipeline/Configuration')
const BuildHistory = () => import(/* webpackChunkName: "g-pipeline" */ '@/components/pipeline/BuildHistory')

const Healthy = () => import(/* webpackChunkName: "g-healthy" */ '@/components/healthy/Healthy')

const Login = () => import(/* webpackChunkName: "g-login" */ '@/components/login/login')

const RealTimeAlm = () => import(/* webpackChunkName: "g-alarm" */ '@/components/alarm/RealTimeAlm')
const HistoryAlm = () => import(/* webpackChunkName: "g-alarm" */ '@/components/alarm/HistoryAlm')

const Credential = () => import(/* webpackChunkName: "g-profile" */ '@/components/user/Credential')
const Profile = () => import(/* webpackChunkName: "g-profile" */ '@/components/user/Profile')

const App = () => import(/* webpackChunkName: "g-app" */ '@/components/app/App')
const lifecycle = () => import(/* webpackChunkName: "g-app" */ '@/components/app/AppLifeCycle')

import * as glbData from '@/js/globalData'
import * as tokenPrc from '@/js/tokenProcess'

const prjRoutes = {
  path: 'project',
  name: glbData.PRJ_MAIN,
  component: Project,
  children: [
    {
      path: ':cfgPrjId/config',
      name: glbData.PRJ_CONFIG,
      component: Project
    }
  ]
}

const pplRoutes = {
  path: ':prjId?/pipeline',
  name: glbData.PPL_MAIN,
  component: Pipeline,
  props: (route) => (
    {prjId: route.params.prjId !== undefined ? parseInt(route.params.prjId) : 0}
  ),
  children: [
    {
      path: ':pplId/detail',
      name: glbData.PPL_DETAIL,
      component: PplDetail,
      redirect: {name: glbData.PPL_BUILDHISTORY},
      children: [
        {
          path: 'configuration',
          name: glbData.PPL_CONFIG,
          component: Configuration
        },
        {
          path: 'build-history',
          name: glbData.PPL_BUILDHISTORY,
          component: BuildHistory
        }
      ]
    }
  ]
}

const appRoutes = {
  path: ':prjId?/app/:appId?',
  name: glbData.APP_MAIN,
  component: App,
  props: (route) => (
    {
      prjId: route.params.prjId !== undefined ? parseInt(route.params.prjId) : 0,
      appId: route.params.appId !== undefined ? parseInt(route.params.appId) : 0
    }
  ),
  redirect: {name: glbData.APP_LIFECYCLE},
  children: [
    {
      path: 'lifecycle',
      name: glbData.APP_LIFECYCLE,
      component: lifecycle
    },
    {
      path: 'alarmstatistic',
      name: glbData.APP_ALARMSTATISTIC,
      component: {template: '<div>Alarm statistic</div>'}
    }
  ]
}

const healthyRoutes = {
  path: 'healthy',
  name: glbData.HLT_MAIN,
  component: Healthy
}

const alarmRoutes = {
  path: 'alarm',
  name: glbData.ALM_MAIN,
  component: {
    template: '<router-view></router-view>'
  },
  redirect: {name: glbData.ALM_REALTIME},
  children: [
    {
      path: 'realtime',
      name: glbData.ALM_REALTIME,
      component: RealTimeAlm
    },
    {
      path: 'history',
      name: glbData.ALM_HISTORY,
      component: HistoryAlm
    }
  ]
}

const loginRoutes = {
  path: 'login',
  name: glbData.LGI_MAIN,
  component: Login
}

const userRoutes = {
  path: 'user',
  name: glbData.USR_MAIN,
  component: {
    template: '<router-view></router-view>'
  },
  redirect: {name: glbData.USR_PROFILE},
  children: [
    {
      path: 'profile',
      name: glbData.USR_PROFILE,
      component: Profile
    },
    {
      path: 'credential',
      name: glbData.USR_CREDENTIAL,
      component: Credential
    }
  ]
}

const mainRoutes = {
  path: 'main',
  name: glbData.MAIN_MAIN,
  component: Main,
  redirect: {name: glbData.PPL_MAIN},
  // redirect: to => {
  //   const {params} = to
  //   params.prjId = 0
  //   return {name: glbData.HLT_MAIN}
  // },
  children: [
    prjRoutes,
    pplRoutes,
    appRoutes,
    healthyRoutes,
    alarmRoutes,
    userRoutes
  ]
}

const routes = [
  {
    path: '/',
    component: {
      template: '<router-view></router-view>'
    },
    redirect: {name: glbData.LGI_MAIN},
    beforeEnter: (to, from, next) => {
      let token = tokenPrc.getToken()
      if (token != null) {
        if (to.name === glbData.LGI_MAIN) {
          tokenPrc.checkToken(token).then(() => {
            next({name: glbData.MAIN_MAIN})
          }).catch(() => {
            next()
          })
        } else next()
      } else {
        if (to.name !== glbData.LGI_MAIN) next({name: glbData.LGI_MAIN})
        else next()
        // next()
      }
    },
    children: [
      loginRoutes,
      mainRoutes
    ]
  },
  {
    path: '/*',
    beforeEnter: (to, from, next) => {
      let direct = {name: glbData.LGI_MAIN}
      let token = tokenPrc.getToken()
      if (token != null) direct = {name: glbData.MAIN_MAIN}
      next(direct)
    }
  }
]

export default routes
