# 1.1.0: Maintenance Release
## [Changed]
    * 添加CHANGELOG
# 1.0.12: Maintenance Release
## [Changed]
    * 修改界面上boolean类型显示不对的问题
# 1.0.11: Maintenance Release
## [Changed]
    * Build的执行结果从jenkins读取，不需要再进行回传
    * url从devopscloud改完codepipeline
# 1.0.10: Maintenance Release
## [Changed]
    * admin用户放开创建pipeline和查看log的权限，guest用户还是只有执行的权限   
# 1.0.9: Maintenance Release
## [Changed]
    * 去掉log功能
# 1.0.8: Maintenance Release
## [Changed]
    * 只留下pipeline界面